Algorithm and JS implementation: dila  
Python port: SmilingWolf  
  
So dila comes one day and shows us this cool thing which takes arbitrary data in input and "trasforms" it in a seemingly (at first sight) meaningful text using Markov chains and a base text.  
I think it is extremely cool, but there is one problem: the JS implementation works only on Chrome. Me being a Firefox guy am like yo man that ain't really cool.  
So I ported the thing to python, so that in 20 years time it will still be working even on your granny's smart toaster running linux.