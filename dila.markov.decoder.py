#!/usr/bin/env python

# Algorithm and JS implementation: dila
# Python port: SmilingWolf

import math, sys

map = {}
state = {}

def get_count(length):
    count = int(math.floor(math.log(length, 2)))
    if (count > 8):
        count = 8
    return count

def write_one_bit(value, state):
    state['dec_byte'] |= value << state['dest_bit']
    state['dest_bit'] += 1
    if (state['dest_bit'] == 8):
        state['dec_out'] += chr(state['dec_byte'])
        state['dec_byte'] = 0
        state['dest_bit'] = 0
        state['dest_index'] += 1

def write_bits(value, count, state):
    for elem in xrange(count):
        write_one_bit((value >> elem) & 1, state)

def decode_once(markovmap, state):
    sys.stdout.write('Decoding: %d%% done\r' % (((state['word_index'] + 2) / float(len(state['src_words']))) * 100))
    sys.stdout.flush()
    word = state['src_words'][state['word_index']]
    state['word_index'] += 1
    choices = markovmap[word]
    count = get_count(len(choices))
    index = choices.index(state['src_words'][state['word_index']])
    write_bits(index, count, state)
    if (state['word_index'] + 1 == len(state['src_words'])):
        return False
    return True

def decoder_init(data, state):
    state['src_words'] = data.split(' ')
    state['dec_out'] = ''
    state['dec_byte'] = 0
    state['word_index'] = 0
    state['dest_index'] = 0
    state['dest_bit'] = 0

if (len(sys.argv) != 4):
    print 'Usage:'
    print 'decoder.py infile keytext outfile'
    quit()

basefile = open(sys.argv[2], 'rb')
basetext = basefile.read()
basefile.close()

basetext = ' '.join(basetext.split('\n'))
words = basetext.split(' ')

for elem in xrange(len(words) - 1):
    if (words[elem] not in map):
        item = []
    else:
        item = map[words[elem]]
    if (words[elem + 1] not in item):
        item.append(words[elem + 1])
    map[words[elem]] = item

encodedfile = open(sys.argv[1], 'rb')
encodedtext = encodedfile.read()
encodedfile.close()

decoder_init(encodedtext, state)

keepgoing = decode_once(map, state)
while (keepgoing != False):
    keepgoing = decode_once(map, state)

outfile = open(sys.argv[3], 'wb')
outfile.write(state['dec_out'])
outfile.close()
