#!/usr/bin/env python

import base64

basefile = open('image.jpeg', 'rb')
cleartext = basefile.read()
basefile.close()

encoded = 'data:image/jpeg;base64,'
encoded += base64.b64encode(cleartext)

basefile = open('image.txt', 'wb')
basetext = basefile.write(encoded)
basefile.close()
