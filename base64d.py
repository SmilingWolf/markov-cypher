#!/usr/bin/env python

import base64

basefile = open('image.txt', 'rb')
basetext = basefile.read()
basefile.close()

image = base64.b64decode(basetext[23:])

basefile = open('image.jpeg', 'wb')
basetext = basefile.write(image)
basefile.close()
