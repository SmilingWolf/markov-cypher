#!/usr/bin/env python

# Algorithm and JS implementation: dila
# Python port: SmilingWolf
# Ain't this stuff cool? I mean, it almost looks
# like the generated encoded text makes sense!

import math, random, sys

map = {}
state = {}

def get_count(length):
    count = int(math.floor(math.log(length, 2)))
    if (count > 8):
        count = 8
    return count

def read_one_bit(state):
    if (state['src_index'] == len(state['src_data'])):
        return 0
    bit = (ord(state['src_data'][state['src_index']]) >> state['src_bit']) & 1
    state['src_bit'] += 1
    if (state['src_bit'] == 8):
        state['src_bit'] = 0
        state['src_index'] += 1
    return bit

def read_bits(count, state):
    ret = 0
    for elem in xrange(count):
        ret |= read_one_bit(state) << elem
    return ret

def encode_once(markovmap, state):
    sys.stdout.write('Encoding: %d%% done\r' % ((state['src_index'] / float(len(state['src_data']))) * 100))
    sys.stdout.flush()
    if (state['src_index'] >= len(state['src_data'])):
        return False
    choices = markovmap[state['last_word']]
    if (len(choices) == 1):
        state['last_word'] = choices[0]
    else:
        count = get_count(len(choices))
        index = read_bits(count, state)
        state['last_word'] = choices[index]
    state['enc_out'] += " " + state['last_word']
    return True

def encoder_init(data, state):
    state['src_data'] = data
    seeds = ['It', 'The', 'He', 'She']
    state['last_word'] = seeds[random.randint(0, len(seeds) - 1)]
    state['enc_out'] = state['last_word']
    state['src_index'] = 0
    state['src_bit'] = 0

if (len(sys.argv) != 4):
    print 'Usage:'
    print 'encoder.py infile keytext outfile'
    quit()
    
basefile = open(sys.argv[2], 'rb')
basetext = basefile.read()
basefile.close()

basetext = ' '.join(basetext.split('\n'))
words = basetext.split(' ')

for elem in xrange(len(words) - 1):
    if (words[elem] not in map):
        item = []
    else:
        item = map[words[elem]]
    if (words[elem + 1] not in item):
        item.append(words[elem + 1])
    map[words[elem]] = item

plainfile = open(sys.argv[1], 'rb')
plaintext = plainfile.read()
plainfile.close()

encoder_init(plaintext, state)

keepgoing = encode_once(map, state)
while (keepgoing != False):
    keepgoing = encode_once(map, state)

outfile = open(sys.argv[3], 'wb')
outfile.write(state['enc_out'])
outfile.close()
